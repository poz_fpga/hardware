EESchema Schematic File Version 4
LIBS:zynq-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L poz_projLib:Zynq-7Z007D U1
U 1 1 5C82BB5E
P 10400 6200
F 0 "U1" H 10275 11125 50  0000 C CNN
F 1 "Zynq-7Z007D" H 10275 11034 50  0000 C CNN
F 2 "POZ_Footprint:CLG225" H 8600 2850 50  0001 C CNN
F 3 "" H 8600 2850 50  0001 C CNN
	1    10400 6200
	1    0    0    -1  
$EndComp
$Comp
L power:+1V8 #PWR0101
U 1 1 5C82BE3E
P 8900 1200
F 0 "#PWR0101" H 8900 1050 50  0001 C CNN
F 1 "+1V8" H 8915 1373 50  0000 C CNN
F 2 "" H 8900 1200 50  0001 C CNN
F 3 "" H 8900 1200 50  0001 C CNN
	1    8900 1200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5C82BEAA
P 9150 1200
F 0 "#PWR0102" H 9150 1050 50  0001 C CNN
F 1 "+3.3V" H 9165 1373 50  0000 C CNN
F 2 "" H 9150 1200 50  0001 C CNN
F 3 "" H 9150 1200 50  0001 C CNN
	1    9150 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5C7FF415
P 9000 14550
F 0 "#PWR0103" H 9000 14300 50  0001 C CNN
F 1 "GND" H 9005 14377 50  0000 C CNN
F 2 "" H 9000 14550 50  0001 C CNN
F 3 "" H 9000 14550 50  0001 C CNN
	1    9000 14550
	1    0    0    -1  
$EndComp
Text GLabel 8900 14500 0    50   Input ~ 0
GND
Wire Wire Line
	9000 14550 9000 14500
Wire Wire Line
	9000 14500 8900 14500
Text GLabel 9100 2500 0    50   Input ~ 0
GND
Text GLabel 9100 3800 0    50   Input ~ 0
GND
Text GLabel 9100 5100 0    50   Input ~ 0
GND
Text GLabel 9100 6100 0    50   Input ~ 0
GND
Text GLabel 9100 6400 0    50   Input ~ 0
GND
Text GLabel 9100 7400 0    50   Input ~ 0
GND
Text GLabel 9100 8500 0    50   Input ~ 0
GND
Text GLabel 9100 8700 0    50   Input ~ 0
GND
Text GLabel 9100 10400 0    50   Input ~ 0
GND
Text GLabel 9100 11300 0    50   Input ~ 0
GND
Text GLabel 9100 11500 0    50   Input ~ 0
GND
Text GLabel 9100 12300 0    50   Input ~ 0
GND
Text GLabel 9100 13200 0    50   Input ~ 0
GND
Text GLabel 9100 13600 0    50   Input ~ 0
GND
Text GLabel 11450 11900 2    50   Input ~ 0
GND
Text GLabel 11450 10600 2    50   Input ~ 0
GND
Text GLabel 11450 9600 2    50   Input ~ 0
GND
Text GLabel 11450 9300 2    50   Input ~ 0
GND
Text GLabel 11450 8300 2    50   Input ~ 0
GND
Text GLabel 11450 7000 2    50   Input ~ 0
GND
Text GLabel 11450 5700 2    50   Input ~ 0
GND
Text GLabel 11450 5100 2    50   Input ~ 0
GND
Text GLabel 11450 4700 2    50   Input ~ 0
GND
Text GLabel 11450 4000 2    50   Input ~ 0
GND
Text GLabel 11450 3700 2    50   Input ~ 0
GND
Text GLabel 11450 3400 2    50   Input ~ 0
GND
Text GLabel 11450 2300 2    50   Input ~ 0
GND
Text GLabel 11450 1900 2    50   Input ~ 0
GND
Text GLabel 8900 1200 3    50   Input ~ 0
1V8
Text GLabel 9150 1200 3    50   Input ~ 0
3V3
$Comp
L power:+5V #PWR?
U 1 1 5C80CE8F
P 8700 1200
F 0 "#PWR?" H 8700 1050 50  0001 C CNN
F 1 "+5V" H 8715 1373 50  0000 C CNN
F 2 "" H 8700 1200 50  0001 C CNN
F 3 "" H 8700 1200 50  0001 C CNN
	1    8700 1200
	1    0    0    -1  
$EndComp
Text GLabel 8700 1200 3    50   Input ~ 0
5V0
$EndSCHEMATC
